package ru.octoteam.octolib;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import kong.unirest.Unirest;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class OctoLib extends JavaPlugin {

    private static final JsonParser p = new JsonParser();

    private static OctoLib inst = null;
    private String authKey, apiEndpoint;

    @Override
    public void onEnable() {
        try {
            saveDefaultConfig();
            authKey = getConfig().getString("auth-key");
            apiEndpoint = getConfig().getString("api-endpoint", "http://localhost:5000/minelink");
            Locale.init(getConfig().getConfigurationSection("lang"));
            if (authKey == null)
                throw new Exception("Need to specify authorization key for upstream server!");
            inst = this;
        } catch (Exception e) {
            e.printStackTrace();
            getPluginLoader().disablePlugin(this);
        }
    }

    private static void sendActionButton(Player ply, String msg, String btn, ClickEvent click, HoverEvent hover) {
        String[] parts = msg.split("@here@");
        List<BaseComponent> comps = new ArrayList<>(Arrays.asList(TextComponent.fromLegacyText(btn)));
        for (BaseComponent comp : comps) {
            comp.setClickEvent(click);
            comp.setHoverEvent(hover);
        }
        List<BaseComponent> res = new ArrayList<>(Arrays.asList(TextComponent.fromLegacyText(parts[0])));
        for (int i = 1; i < parts.length; ++i) {
            res.addAll(comps);
            res.addAll(Arrays.asList(TextComponent.fromLegacyText(parts[i])));
        }
        ply.sendMessage(res.toArray(new BaseComponent[0]));
    }

    private static void requestAuth(Player ply, String token) {
        sendActionButton(ply, Locale.get("auth"), Locale.get("here"),
                new ClickEvent(ClickEvent.Action.OPEN_URL, inst.apiEndpoint + "?token=" + token),
                new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Locale.get("click-to-open"))));
    }

    private static void runAsync(Runnable r) {
        Bukkit.getScheduler().runTaskAsynchronously(inst, r);
    }
    private static void run(Runnable r) {
        Bukkit.getScheduler().runTask(inst, r);
    }

    public static void getUser(Player ply, Consumer<User> result, boolean requireOnline) throws NotImplementedException {
        if (inst == null) throw new NotImplementedException("Lib was not initialized");
        String id = ply.getName().toLowerCase();
        runAsync(() -> {
            JsonObject obj = p.parse(Unirest.get(inst.apiEndpoint + "/admin/getuser")
                    .queryString("userID", id)
                    .header("Authorization", inst.authKey)
                    .asString().getBody()).getAsJsonObject();
            if (obj.get("steamID").isJsonNull()) {
                run(() -> {
                    if (ply.isOnline()) {
                        ply.sendMessage(Locale.get("not-linked"));
                        requestAuth(ply, obj.get("token").getAsString());
                    }
                });
                return;
            }
            run(() -> {
                if (ply.isOnline() || !requireOnline)
                    result.accept(new User(obj));
            });
        });
    }
    public static void getUser(Player ply, Consumer<User> result) throws NotImplementedException {
        getUser(ply, result, true);
    }

    public static void unlink(Player ply, Consumer<Boolean> result, boolean requireOnline) throws NotImplementedException {
        if (inst == null) throw new NotImplementedException("Lib was not initialized");
        String id = ply.getName().toLowerCase();
        runAsync(() -> {
            JsonObject obj = p.parse(Unirest.get(inst.apiEndpoint + "/admin/unlink")
                    .queryString("userID", id)
                    .header("Authorization", inst.authKey)
                    .asString().getBody()).getAsJsonObject();
            run(() -> {
                if (ply.isOnline() || !requireOnline)
                    result.accept(!obj.has("error"));
            });
        });
    }
    public static void unlink(Player ply, Consumer<Boolean> result) throws NotImplementedException {
        unlink(ply, result, true);
    }

    public static void takeMoney(Player ply, int amount, Runnable result) throws NotImplementedException {
        if (inst == null) throw new NotImplementedException("Lib was not initialized");
        if (amount <= 0) return;
        String id = ply.getName().toLowerCase();
        getUser(ply, (u) -> {
            if (u.balance < amount) {
                if (ply.isOnline())
                    ply.sendMessage(Locale.get("not-enough-money"));
                return;
            }
            runAsync(() -> {
                JsonObject obj = p.parse(Unirest.get(inst.apiEndpoint + "/admin/spend")
                        .queryString("userID", id)
                        .queryString("amount", amount)
                        .header("Authorization", inst.authKey)
                        .asString().getBody()).getAsJsonObject();
                run(() -> {
                    if (obj.has("error")) {
                        if (ply.isOnline())
                            ply.sendMessage(Locale.get("error-occurred", obj.get("error").getAsString()));
                        return;
                    }
                    result.run();
                });
            });
        }, false);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            inst = null;
            reloadConfig();
            authKey = getConfig().getString("auth-key");
            apiEndpoint = getConfig().getString("api-endpoint", "http://localhost:5000/minelink");
            getLogger().info("Using API endpoint " + apiEndpoint);
            if (authKey == null) {
                getLogger().severe("Need to specify authorization key for upstream server!");
                getPluginLoader().disablePlugin(this);
                return true;
            } else inst = this;
            getLogger().info("Config and locale reloaded successfully");
            return true;
        }
        Player ply = (Player) sender;
        try {
            if (args.length > 0 && args[0].equalsIgnoreCase("unlink")) {
                unlink(ply, (ans) -> ply.sendMessage(Locale.get("unlink-" + (ans ? "succ" : "fail"))));
                return true;
            }
            if (args.length > 0 && args[0].equalsIgnoreCase("test")) {
                takeMoney(ply, 10, () -> {
                    if (ply.isOnline())
                        ply.sendMessage("success!!");
                });
                return true;
            }
            getUser(ply, (user) -> {
                ply.sendMessage(Locale.get("attached-to", user.steamID));
                sendActionButton(ply, Locale.get("unlink-hint"), Locale.get("here"),
                        new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/steam unlink"),
                        new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Locale.get("click-to-unlink"))));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
