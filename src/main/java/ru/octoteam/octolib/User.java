package ru.octoteam.octolib;

import com.google.gson.JsonObject;

public class User {
    public final String userID, steamID, token;
    public final int balance;

    public User(JsonObject obj) {
        this.userID = obj.get("userID").getAsString();
        this.steamID = obj.get("steamID").getAsString();
        this.token = obj.get("token").getAsString();
        this.balance = obj.get("balance").getAsInt();
    }
}
