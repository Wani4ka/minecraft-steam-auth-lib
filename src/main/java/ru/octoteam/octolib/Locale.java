package ru.octoteam.octolib;

import org.bukkit.configuration.ConfigurationSection;

public class Locale {
    private static ConfigurationSection lc;
    public static void init(ConfigurationSection lc) throws Exception {
        if (lc == null)
            throw new Exception("Lang section was not found or invalid");
        Locale.lc = lc;
    }
    public static String get(String key, Object... args) {
        String s = lc.getString(key, "???").replaceAll("&([0-9a-fk-or])", "§$1");
        return String.format(s, args);
    }
}
